# firewatch
A fluxbox and i3 style with colors based on the color scheme of the Firewatch website ( http://www.firewatchgame.com/ )

The wallpaper was found on reddit and is not my own ( http://www.reddit.com/r/wallpaper+wallpapers/search?q=firewatch&restrict_sr=on&sort=relevance&t=all)

The archive was designed to be extracted in your home directory and work, with the exception of the i3 files and the .Xresources file. These were renamed to prevent the overwriting of the user's existing files.

Firefox theme is firefoxgfx: https://addons.mozilla.org/En-us/firefox/addon/firefff/
